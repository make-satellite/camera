Benutzte Kamera Bibliothek:
https://github.com/yoursunny/esp32cam

Mögliche Sensor Einstellungen:
https://github.com/espressif/esp32-camera/blob/master/driver/include/sensor.h

Link zu Aliexpress:
https://www.aliexpress.com/item/33033854675.html?spm=a2g0s.9042311.0.0.172d4c4dwEYHgV

Beispiel deep sleep:
https://randomnerdtutorials.com/esp32-deep-sleep-arduino-ide-wake-up-sources/

Beispiel zum schreiben auf Sdkarte und deep sleep:
https://randomnerdtutorials.com/esp32-cam-take-photo-save-microsd-card/

ESP 32 Datasheet:
https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_en.pdf

Zum Programmieren und flashen des ESP32 benutze ich 
VisualStudioCode: https://code.visualstudio.com/
und die Erweiterung PlatformIO:
https://platformio.org/
https://platformio.org/install/ide?install=vscode
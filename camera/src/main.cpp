#include <Arduino.h>
#include <esp32cam.h>

// Avaible resolutions
static auto QQVGA = esp32cam::Resolution::find(160, 120);
static auto QQVGA2 = esp32cam::Resolution::find(128, 160);
static auto QCIF = esp32cam::Resolution::find(176, 144);
static auto HQVGA = esp32cam::Resolution::find(240, 176);
static auto QVGA = esp32cam::Resolution::find(320, 240);
static auto CIF = esp32cam::Resolution::find(400, 296);
static auto VGA = esp32cam::Resolution::find(640, 480);
static auto SVGA = esp32cam::Resolution::find(800, 600);
static auto XGA = esp32cam::Resolution::find(1024, 768);
static auto SXGA = esp32cam::Resolution::find(1280, 1024);
static auto UXGA = esp32cam::Resolution::find(1600, 1200);
static auto QXGA = esp32cam::Resolution::find(2048, 1536);

// Quality of the JPEG
static int QUALITY = 80;

void setup()
{
	Serial.begin(115200);
	Serial.println();

	// Set LED Flash pin
	pinMode(4, OUTPUT);
	digitalWrite(4, LOW);

	using namespace esp32cam;
	Config cfg;
	cfg.setPins(pins::AiThinker);

	// JPEG settings
	cfg.setResolution(QQVGA);
	cfg.setBufferCount(2);
	cfg.setJpeg(QUALITY);

	// Set to grayscale
	cfg.setGrayscale();

	Serial.println("CAM init start");
	bool ok = Camera.begin(cfg);
	Serial.println(ok ? "CAMERA OK" : "CAMERA FAIL");

	// Turn flash on, capture image, turn flash off
	Serial.println("capuring frame");
	digitalWrite(4, HIGH);
	auto frame = esp32cam::capture();
	digitalWrite(4, LOW);

	// Convert the frame to JPEG
	frame->toJpeg(QUALITY);

	Serial.print("size: ");
	Serial.println(static_cast<int>(frame->size()));

	// Send the bytes over serial
	Serial.print("|");
	auto frameBytes = frame->data();
	for (size_t i = 0; i < frame->size(); i++, frameBytes++)
	{
		auto currentByte = *frameBytes;

		// Padding
		if (currentByte < 16)
			Serial.print("0");

		Serial.print(currentByte, HEX);
		Serial.print(" ");
	}
	Serial.println("|");

	Serial.println("frame capured");
}

void loop()
{
	// put your main code here, to run repeatedly:
}
#include <Arduino.h>
#include <esp32cam.h>
#include <WebServer.h>
#include <WiFi.h>

const char *SSID = "my WLAN network name";
const char *PASSWORD = "my WLAN password";

// Avaible resolutions
static auto QQVGA = esp32cam::Resolution::find(160, 120);
static auto QQVGA2 = esp32cam::Resolution::find(128, 160);
static auto QCIF = esp32cam::Resolution::find(176, 144);
static auto HQVGA = esp32cam::Resolution::find(240, 176);
static auto QVGA = esp32cam::Resolution::find(320, 240);
static auto CIF = esp32cam::Resolution::find(400, 296);
static auto VGA = esp32cam::Resolution::find(640, 480);
static auto SVGA = esp32cam::Resolution::find(800, 600);
static auto XGA = esp32cam::Resolution::find(1024, 768);
static auto SXGA = esp32cam::Resolution::find(1280, 1024);
static auto UXGA = esp32cam::Resolution::find(1600, 1200);
static auto QXGA = esp32cam::Resolution::find(2048, 1536);

WebServer server(80);

// Quality of the JPEG
static int QUALITY = 80;

void handlejpeg()
{
	if (server.hasArg("quality"))
	{
		auto data = server.arg("quality");

		if (data == "QQVGA")
		{
			if (!esp32cam::Camera.changeResolution(QQVGA))
				Serial.println("SET QQVGA FAIL");
		}
		else if (data == "QQVGA2")
		{
			if (!esp32cam::Camera.changeResolution(QQVGA2))
				Serial.println("SET QQVGA2 FAIL");
		}
		else if (data == "QCIF")
		{
			if (!esp32cam::Camera.changeResolution(QCIF))
				Serial.println("SET QCIF FAIL");
		}
		else if (data == "HQVGA")
		{
			if (!esp32cam::Camera.changeResolution(HQVGA))
				Serial.println("SET HQVGA FAIL");
		}
		else if (data == "QVGA")
		{
			if (!esp32cam::Camera.changeResolution(QVGA))
				Serial.println("SET QVGA FAIL");
		}
		else if (data == "CIF")
		{
			if (!esp32cam::Camera.changeResolution(CIF))
				Serial.println("SET CIF FAIL");
		}
		else if (data == "VGA")
		{
			if (!esp32cam::Camera.changeResolution(VGA))
				Serial.println("SET VGA FAIL");
		}
		else if (data == "SVGA")
		{
			if (!esp32cam::Camera.changeResolution(SVGA))
				Serial.println("SET SVGA FAIL");
		}
		else if (data == "XGA")
		{
			if (!esp32cam::Camera.changeResolution(XGA))
				Serial.println("SET XGA FAIL");
		}
		else if (data == "SXGA")
		{
			if (!esp32cam::Camera.changeResolution(SXGA))
				Serial.println("SET SXGA FAIL");
		}
		else if (data == "UXGA")
		{
			if (!esp32cam::Camera.changeResolution(UXGA))
				Serial.println("SET UXGA FAIL");
		}
		else if (data == "QXGA")
		{
			if (!esp32cam::Camera.changeResolution(QXGA))
				Serial.println("SET QXGA FAIL");
		}
	}

	auto frame = esp32cam::capture();
	if (frame == nullptr)
	{
		Serial.println("CAPTURE FAILED");
		server.send(503, "plain/text", "capture failed");
		return;
	}

	Serial.printf("CAPTURE OK %dx%d %db\n", frame->getWidth(), frame->getHeight(),
				  static_cast<int>(frame->size()));

	server.setContentLength(frame->size());
	server.send(200, "image/jpeg");
	WiFiClient client = server.client();
	frame->writeTo(client);
}

void setup()
{
	Serial.begin(115200);
	Serial.println();

	// Set LED Flash pin
	pinMode(4, OUTPUT);
	digitalWrite(4, LOW);

	using namespace esp32cam;
	Config cfg;
	cfg.setPins(pins::AiThinker);

	// JPEG settings
	// Set the highest possible/desired resolution here, because this will set the max memory buffer
	// QXGA and UXGA didnt worked in my ESP
	cfg.setResolution(SXGA);
	cfg.setBufferCount(2);
	cfg.setJpeg(QUALITY);

	Serial.println("CAM init start");
	bool ok = Camera.begin(cfg);
	Serial.println(ok ? "CAMERA OK" : "CAMERA FAIL");

	WiFi.persistent(false);
	WiFi.mode(WIFI_STA);
	WiFi.begin(SSID, PASSWORD);
	while (WiFi.status() != WL_CONNECTED)
	{
		digitalWrite(4, HIGH);
		delay(500);
		digitalWrite(4, LOW);
		delay(500);
	}

	digitalWrite(4, LOW);
	Serial.print("Connected to ");
	Serial.print(SSID);
	Serial.print(" with ");
	Serial.println(WiFi.localIP());

	server.on("/jpeg", handlejpeg);
	server.begin();
}

void loop()
{
	server.handleClient();
}
﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace BytesToImg
{
    static class Program
    {
        private static SerialPort serialPort;
        private static bool buffering;
        private static StringBuilder buffer = new StringBuilder();

        static void Main(string[] args)
        {
            Console.WriteLine("Select one of the following serial ports:");
            Console.WriteLine(string.Join(' ', SerialPort.GetPortNames()));

            var input = Console.ReadLine().Trim();

            if (!input.StartsWith("COM"))
                input = "COM" + input;

            serialPort = new SerialPort(input)
            {
                // Set the readbuffer to the max value because the images can get quite big
                ReadBufferSize = int.MaxValue - 1,

                // Configured in the ESP32 program code
                BaudRate = 115200
            };

            serialPort.DataReceived += SerialPort_DataReceived;
            serialPort.Open();

            Console.WriteLine("Press enter to exit..");
            Console.ReadLine();

            serialPort.Close();
            serialPort.Dispose();
        }

        private static void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var data = serialPort.ReadExisting();

            foreach (var line in data.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                // In order to keep the data separate from the logs, it must start and end with a |
                // If the image is small enough to be in one serial read
                if (line.StartsWith("|") && line.EndsWith("|"))
                {
                    HandleImage(line.Trim('|'));
                    continue;
                }

                // If the data is to big to be captured in a single serial read, we buffer the received data
                if (line.StartsWith("|"))
                    buffering = true;
                else if (line.EndsWith("|"))
                    buffering = false;

                if (buffering)
                {
                    buffer.Append(line);
                }
                else if (buffer.Length > 0)
                {
                    buffer = buffer.Replace("\r\n", "");
                    HandleImage(buffer.ToString().Trim('|'));
                    buffer.Clear();
                }
                else
                {
                    Console.WriteLine(line);
                }
            }
        }

        private static void HandleImage(string line)
        {
            // Data consists of a .jpeg as a byte array which has been decoded in hexadecimal
            var bytes = line
                .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => byte.Parse(x, NumberStyles.HexNumber))
                .ToArray();

            using var stream = new MemoryStream(bytes);
            using var img = Image.FromStream(stream);
            var name = $"{DateTime.Now.ToString("HH-mm-ss")}_img.jpeg";
            img.Save(name, ImageFormat.Jpeg);

            Console.WriteLine($"Image saved! {name}");
        }
    }
}
